import java.io.IOException;

/**
 * Diese Klasse gibt je nach Eingabe des Arguments eine passende Fehlermeldung aus. 
 * Bei einer korrekten Eingabe erscheint eine Bestaetigung und das Interface wird generiert.
 * 
 * @author Stephanie Kaswurm und Anna-Lena Klaus
 */

public class Main {

	public static void main(String[] args) throws ClassNotFoundException, IOException {
		
		//Wenn das mitgegebene Argument nicht mitgegeben wurde, wird eine Fehlermeldung ausgegeben.
		 if(args.length != 1) {
			System.out.println(" Fehlendes Argument: Bitte gib einen gueltigen Klassennamen ein \n oder gib den richtigen Klassenpfad an.<Java-Package.Klassenname> \n Bsp: java.lang.Boolean.");
			System.exit(0);
		}
		 
		 
		 Class<?> clazz = existiertKlasse(args[0]);
		 
		 	/* Wenn der Klassenname existiert wird ein Interface generiert, ansonsten wird
		 	 * darauf verwiesen, dass ein gueltiger Klassenname anzugeben ist. 
		 	 */
		 
			if (clazz != null) {
				Generator generator = new Generator();
				generator.InterfaceGenerator(clazz);
				System.out.println("Das Interface wurde generiert und ist im root des Projektes abgelegt.");
			
				/* Wenn beim Argument ein Fehler passiert ist, wird man aufgefordert, einen
				 * gueltigen Klassennamen mit dem dazugehoerigen Klassenpfad anzugeben.
				 */
				
			} else {
				System.out.println("Bitte gib einen gueltigen Klassennamen mit dem dazugehoerigen Klassenpfad an.");
			}
		}

		/*
		 * Diese Methode ueberprueft, ob die angebene Klasse wirklich existiert. 
		 */
	
		private static Class<?> existiertKlasse(String classname) {
			Class<?> c;
			try {
				c = Class.forName(classname);
			} catch (ClassNotFoundException e) {
				c = null;
			}
			return c;
		}		
}
		
		
		
	
