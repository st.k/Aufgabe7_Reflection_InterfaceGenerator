
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Die Annotation "Todo" kennt die Elemente what und when vom Typ String und int.
 * - when ist eine Relesenummer
 * - what bezeichnet zukuenftige Aufgaben in Release.
 * 
 * @author Anna-Lena Klaus, Stephanie Kaswurm
 *
 */

/* Dadurch ist die Annotation auch waehrend der Laufzeit verfuegbar, 
 * damit Tests mittels Reflection gemacht werden koennen.
 */

@Retention(RetentionPolicy.RUNTIME)

/* Die Annotation kann nur auf Enums, Annotations, Klassen, 
 * Interfaces und Methoden angewendet werden. 
 */

@Target({ElementType.METHOD, ElementType.TYPE})


public @interface Todo {

	String what();
	int when();
	
		
}
