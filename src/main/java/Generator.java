import java.io.IOException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Diese Klasse erstellt einen Interface-Generator einer ueber ein Argument eingegebenen Klasse.
 * Dabei wird beim Kopf des generierten Interfaces ein If vor den Klassennamen angehaengt.
 * Im Rumpf werden alle Methoden (public) und deren Parametertypen aufgelistet und in ein File
 * geschrieben.
 *
 * @author Stephanie Kaswurm und Anna-Lena Klaus
 *
 */

public class Generator {
	
	//Es wird eine Arraylist erstellt, in welche spaeter das Interface eingefuegt wird.
	List<String> lines;
	
	public Generator() {
		lines = new ArrayList<>();
	}

	
	/*
	 * Der Interface Generator generiert ein Interface der angegebenen Klasse und
	 * schreibt dieses in ein File.
	 */
		
	public void InterfaceGenerator(Class<?> clazz) throws ClassNotFoundException{
		
		//Der Kopf des Interfaces wird mit "public interface If" vor dem Klassennamen generiert.		
		lines.add("public interface If" + clazz.getSimpleName() + " {");

		Method[] methods = clazz.getDeclaredMethods();

		// Die For-Each-Schleife gibt alle Methoden aus.
		for (Method method : methods) {

			if (Modifier.isPublic(method.getModifiers())) {
				String parameterString = "";
				
				//Alle dazugehoerigen Parametertypen werden zugewiesen.
				Class<?>[] parameterTypes = method.getParameterTypes();

				for (int i = 0; i < parameterTypes.length; i++) {
					Class<?> parameterType = parameterTypes[i];
					parameterString += parameterType.getName() + " arg" + i;

					//Um die Parameter schoen zu trennen, werden Kommas gesetzt.
					if (i < parameterTypes.length - 1) {
						parameterString += ", ";
					}
				}
				
				
				//Alle Methoden werden nacheinander mit ihren Parametern ausgegeben und in das File geschrieben.
				lines.add("    public " + method.getReturnType().getName() + " " + method.getName() + "("
						+ parameterString + ");");
			}
		}

		lines.add("}");
	
			// Das File wird korrekt benannt --> Muss gleich heissen wie das Interface.
			Path file = Paths.get("If" + clazz.getSimpleName() + ".java");
			try {
				Files.write(file, lines);
			} catch (IOException e) {
				e.printStackTrace();
			}
	}
}